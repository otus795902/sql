﻿--liquibase formatted sql

create table visit
(
    student_id uuid
        constraint visit_student_id_fk
            references public.student,
    lesson_id  uuid
        constraint visit_lesson_id_fk
            references public.lesson
);

alter table visit
    owner to postgres;