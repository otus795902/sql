﻿--liquibase formatted sql

create table student
(
    id    uuid default gen_random_uuid() not null
        constraint student_pk
            primary key,
    name  varchar                        not null,
    skill int,
    curator_id uuid
);

alter table student
    owner to postgres;