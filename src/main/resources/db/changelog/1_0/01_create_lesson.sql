﻿--liquibase formatted sql

create table lesson
(
    id        uuid default gen_random_uuid() not null
        constraint lesson_pk
            primary key,
    name      varchar                        not null,
    date_time timestamp with time zone
);

alter table lesson
    owner to postgres;