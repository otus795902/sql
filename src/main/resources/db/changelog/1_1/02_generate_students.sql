﻿--liquibase formatted sql

create or replace function generate_name()
    returns text
as
'
    DECLARE
        name text;
    BEGIN
        SELECT CONCAT(
                               (ARRAY [''Алексей'', ''Борис'', ''Владимир'', ''Григорий'', ''Дмитрий'', ''Егор'', ''Жора'', ''Зураб''])[FLOOR(RANDOM() * 8 + 1)],
                               '' '',
                               (ARRAY [''Ашкин'', ''Бэшкин'', ''Вэшкин'', ''Гэшкин'', ''Дэшкин'', ''Ешкин'', ''Ёшкин''])[FLOOR(RANDOM() * 7 + 1)])
                INTO name;
        RETURN name;
    END;
'
    language plpgsql;

create or replace function generate_skill()
    returns integer
as
'
    DECLARE skill int;
    BEGIN
        SELECT FLOOR(RANDOM() * 99 + 1)::INT INTO skill;
        RETURN skill;
    END;
'
    language plpgsql;

CREATE OR REPLACE FUNCTION get_random_student_id()
    RETURNS uuid
AS
'
    DECLARE
        students_count    int;
        random_row_number int;
        random_student_id uuid;
    BEGIN
        SELECT COUNT(id)
        FROM student
        INTO students_count;

        SELECT floor(random() * (students_count - 1 + 1) + 1)
        INTO random_row_number;

        SELECT id
        FROM (SELECT id, row_number() OVER () as row_number FROM student) AS students_enumerated
        WHERE row_number = random_row_number INTO random_student_id;

        RETURN random_student_id;
    END;
'
    LANGUAGE plpgsql
    STRICT;

create or replace procedure generate_students(amount int)
as
'
    BEGIN
        FOR i IN 1..amount
            LOOP
                INSERT INTO student (name, skill, curator_id)
                VALUES (GENERATE_NAME(), GENERATE_SKILL(), GET_RANDOM_STUDENT_ID());
            END LOOP;
    END;
'
    language plpgsql;

call generate_students(1000);