﻿--liquibase formatted sql


create or replace procedure generate_visits(amount int)
as
'
    BEGIN
        FOR i IN 1..amount
            LOOP
                INSERT INTO visit(student_id, lesson_id)
                VALUES (GET_RANDOM_STUDENT_ID(), GET_RANDOM_LESSON_ID());
            END LOOP;
    END;
'
    language plpgsql;

call generate_visits(100);