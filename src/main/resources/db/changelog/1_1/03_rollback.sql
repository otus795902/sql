﻿--liquibase formatted sql

DELETE FROM visit LIMIT 10000;

DROP PROCEDURE IF EXISTS generate_visits;
DROP FUNCTION IF EXISTS get_random_student_id;
DROP FUNCTION IF EXISTS get_random_lesson_id;