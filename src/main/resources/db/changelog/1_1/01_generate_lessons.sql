﻿--liquibase formatted sql

CREATE OR REPLACE FUNCTION get_random_lesson_id()
    RETURNS uuid
AS
'
    DECLARE
        lessons_count    int;
        random_row_number int;
        random_lesson_id uuid;
    BEGIN
        SELECT COUNT(id)/2::INT
        FROM lesson
        INTO lessons_count;

        SELECT floor(random() * (lessons_count - 1 + 1) + 1)
        INTO random_row_number;

        SELECT id
        FROM (SELECT id, row_number() OVER () as row_number FROM lesson) AS lessons_enumerated
        WHERE row_number = random_row_number INTO random_lesson_id;

        RETURN random_lesson_id;
    END;
'
    LANGUAGE plpgsql
    STRICT;

create or replace procedure generate_lessons(
    amount int,
    lesson_name varchar
)
    language plpgsql
as
'
begin
    for i in 1..amount
        loop
            insert into lesson (name) values (lesson_name);
        end loop;
end;';

call generate_lessons(1, 'greeting');
call generate_lessons(3, 'sql');
call generate_lessons(6, 'jdbc');