﻿--liquibase formatted sql

DELETE FROM generate_lessons WHERE name = 'jdbc' LIMIT 6;
DELETE FROM generate_lessons WHERE name = 'sql' LIMIT 3;
DELETE FROM generate_lessons WHERE name = 'greeting' LIMIT 1;

DROP PROCEDURE IF EXISTS generate_lessons;