create database otus;

create user application;

grant connect on database otus to application;

create user liquibase;

grant connect on database otus to liquibase;

grant create on database otus to liquibase;