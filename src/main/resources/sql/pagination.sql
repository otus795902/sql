SELECT *
FROM student
WHERE skill > 50
ORDER BY skill
LIMIT 9 OFFSET 0;

SELECT COUNT(*) / 9 +
       CASE WHEN MOD(COUNT(*), COUNT(*) / 9) > 0 THEN 1 ELSE 0 END AS total_pages
FROM student
WHERE skill > 50

