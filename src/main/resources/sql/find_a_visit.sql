-- Пример поиска конкретного студента из посещавших занятия
SELECT row_to_json(visits) AS my_json
FROM (SELECT student.id    AS student_id,
             student.name  AS student_name,
             student.skill AS student_skill,
             lesson.id     AS lesson_id,
             lesson.name   AS lesson_name
      FROM visit
               LEFT JOIN student ON visit.student_id = student.id
               LEFT JOIN lesson ON visit.lesson_id = lesson.id
      WHERE student.name LIKE ?
      LIMIT 1) AS visits;