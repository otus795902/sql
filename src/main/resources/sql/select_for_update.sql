-- Блокируем запись на время транзакции (автоподтверждение включено)

SELECT * FROM student WHERE name = 'Отус Отус' FOR UPDATE SKIP LOCKED;
UPDATE student SET skill = 0 WHERE name = 'Отус Отус';