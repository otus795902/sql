-- LEFT INCLUSIVE
-- Все студенты, плюс некоторые по несколько посещений
SELECT *
FROM student
         LEFT OUTER JOIN visit ON student.id = visit.student_id;

-- RIGHT INCLUSIVE
-- Все посещения. В нашем случае они все со студентами.
SELECT *
FROM student
         RIGHT OUTER JOIN visit ON student.id = visit.student_id;

-- LEFT EXCLUSIVE
-- Все студенты без посещений
SELECT *
FROM student
         LEFT OUTER JOIN visit ON student.id = visit.student_id
WHERE visit.student_id IS NULL;

-- RIGHT EXCLUSIVE
-- Посещение без студентов, у нас таких нет
SELECT *
FROM student
         RIGHT OUTER JOIN visit ON student.id = visit.student_id
WHERE student.id IS NULL;

-- FULL OUTER INCLUSIVE
-- Все комбинации студентов и посещений. В нашем случае совпадает с LEFT INCLUSIVE, т.к. нет посещений без студентов.
SELECT *
FROM student
         FULL OUTER JOIN visit ON student.id = visit.student_id;

-- FULL OUTER EXCLUSIVE
-- Все студенты без занятий плюс занятия без студентов. Совпадает с LEFT EXCLUSIVE, т.к. нет посещений без студентов.
SELECT *
FROM student
         FULL OUTER JOIN visit ON student.id = visit.student_id
WHERE student.id IS NULL OR visit.student_id IS NULL;

-- INNER JOIN
-- Все студенты, у которых есть посещения. В нашем случае совпадает с RIGHT INCLUSIVE, т.к. все посещения со студентами.
SELECT *
FROM student
         INNER JOIN visit ON student.id = visit.student_id;

-- CROSS JOIN - CARTESIAN PRODUCT
-- Все возможные комбинации студентов и уроков. Реальные связи игнорируеются.
SELECT *
FROM student
         CROSS JOIN lesson;

-- SELF JOIN
-- У некоторых студентов есть кураторы из числа студентов.
SELECT *
FROM student
         LEFT OUTER JOIN student AS curator ON student.curator_id = curator.id
WHERE student.curator_id IS NOT NULL;




