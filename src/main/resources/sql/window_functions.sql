-- Группировка через GROUP BY
-- Суммарная сила умений студентов на каждом уроке. Сначала группируем, потом вычисляем.
SELECT lesson_id, sum(skill) AS sum
FROM visit
         LEFT JOIN student ON visit.student_id = student.id
         LEFT JOIN lesson ON visit.lesson_id = lesson.id
GROUP BY lesson_id
ORDER BY sum DESC;

-- Фильтрация по HAVING
-- Уроки, на которых было больше 20 разных студентов
SELECT lesson.id, lesson.name, count(student.id) AS group_students_count
FROM visit
         LEFT JOIN student ON visit.student_id = student.id
         LEFT JOIN lesson ON visit.lesson_id = lesson.id
GROUP BY lesson.id, lesson.name
HAVING count(student.id) > 20;

-- Вычисление с группировкой по значению и агрегацией по каждой группе
-- Средний уровень умений учащихся, посещавших тот или иной урок
SELECT DISTINCT lesson.id,
                lesson.name,
                AVG(skill) OVER (PARTITION BY lesson_id) AS lesson_students_average_skill
FROM visit
         LEFT JOIN student ON visit.student_id = student.id
         LEFT JOIN lesson ON visit.lesson_id = lesson.id
ORDER BY lesson_students_average_skill DESC;

-- Нумерация (ROW_NUMBER)
-- Сквозной рейтинг всех студентов по умениям
SELECT name,
       skill,
       ROW_NUMBER() OVER (ORDER BY skill) AS place
FROM student;

-- Ранжирование (RANK & DENSE_RANK)
-- Рейтинг студентов в рамках каждого урока (с пропуском общих мест и без пропуска)
SELECT DISTINCT lesson.name,
                student.name,
                skill,
                RANK() OVER (PARTITION BY lesson_id ORDER BY skill DESC)       AS rank,
                DENSE_RANK() OVER (PARTITION BY lesson_id ORDER BY skill DESC) AS dense_rank
FROM visit
         LEFT JOIN student ON visit.student_id = student.id
         LEFT JOIN lesson ON visit.lesson_id = lesson.id
ORDER BY lesson.name, rank;

-- Отклонение и распределение (CUME_DIST & PERCENT_RANK)
-- Доля умения студента от максимального и позиция в статистическом распределении
SELECT name,
       skill,
       CUME_DIST() OVER (ORDER BY skill)    max_factor,
       PERCENT_RANK() OVER (ORDER BY skill) distribution_factor
FROM student
ORDER BY max_factor DESC;

-- Крайнее значение (FIRST_VALUE & LAST_VALUE & NTH_VALUE)
-- Тройка лучших студентов по каждому уроку
SELECT DISTINCT lesson.id,
                lesson.name,
                student.name,
                skill,
                FIRST_VALUE(student.name) OVER (ORDER BY skill DESC) AS first_student,
                FIRST_VALUE(student.name)
                OVER (PARTITION BY lesson.id ORDER BY skill DESC)    AS first_lesson_student,
                NTH_VALUE(student.name, 2)
                OVER (PARTITION BY lesson.id ORDER BY skill DESC)    AS second_lesson_student,
                NTH_VALUE(student.name, 3)
                OVER (PARTITION BY lesson.id ORDER BY skill DESC)    AS third_lesson_student,
                LAST_VALUE(student.name)
                OVER (PARTITION BY lesson.id ORDER BY skill DESC RANGE BETWEEN
                    UNBOUNDED PRECEDING AND
                    UNBOUNDED FOLLOWING)                             AS the_worst_lesson_student
FROM visit
         LEFT JOIN student ON visit.student_id = student.id
         LEFT JOIN lesson ON visit.lesson_id = lesson.id
ORDER BY lesson.id, lesson.name, skill DESC;

-- Значение равноудаленной записи (LAG & LEAD)
-- Следующий по умению студент. Поскольку сортировку инвертировали, то и сдвиг инвертировали (-1).
SELECT name,
       skill,
       LAG(name, -1) OVER (ORDER BY skill DESC)  next_by_skill_student,
       LEAD(name, -1) OVER (ORDER BY skill DESC) previous_by_skill_student
FROM student;

-- Разбить на группы (NTILE)
-- Разбить студентов на 10 групп по умениям
SELECT name,
       skill,
       NTILE(10) OVER (ORDER BY skill) AS group_number
FROM student;

-- Строки в массив
-- Текстовый массив с произвольной подстановкой значений типа CSV
SELECT ARRAY_AGG('урок ' || lesson.name || ' - студент ' || student.name || ' - рейтинг ' ||
                 student.skill
           -- ORDER BY first_name ASC, last_name DESC
       ) actors
FROM visit
         LEFT JOIN student ON visit.student_id = student.id
         LEFT JOIN lesson ON visit.lesson_id = lesson.id;

-- Прикрепленная строка как одно из полей:
-- Выборку данных по посещениям в JSON
SELECT row_to_json(visits) AS my_json
FROM (SELECT student.id    AS student_id,
             student.name  AS student_name,
             student.skill AS student_skill,
             lesson.id     AS lesson_id,
             lesson.name   AS lesson_name
      FROM visit
               LEFT JOIN student ON visit.student_id = student.id
               LEFT JOIN lesson ON visit.lesson_id = lesson.id) AS visits;