docker pull postgres
docker volume create postgres_data
docker run --restart always --name postgres_container -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 -v postgres_data:/var/lib/postgresql/data postgres