package ru.otus.sql;

import static java.nio.charset.StandardCharsets.UTF_8;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import java.util.Scanner;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;

@org.springframework.stereotype.Service
@Slf4j
@RequiredArgsConstructor
public class Service {

    private static final String SQL_PATTERN = readFile(Service.class, "/sql/find_a_visit.sql");

    private final EntityManager entityManager;

    @Scheduled(cron = "*/5 * * * * *")
    public void printStudentFreshState() {
        log.info(findAStudent("%Алексей%"));
    }

    private static String readFile(Class<?> callerClass, String fileName) {
        return new Scanner(callerClass.getResourceAsStream(fileName), UTF_8)
            .useDelimiter("\\A")
            .next();
    }

    @SneakyThrows
    private String findAStudent(String namePart) {
        Query query = entityManager.createNativeQuery(SQL_PATTERN);
        query.setParameter(1, namePart);
        Object result = query.getSingleResult();
        return mapToJson(result);
    }

    private static String mapToJson(Object result) throws JsonProcessingException {
        JsonNode json = new ObjectMapper().readTree(String.valueOf(result));
        return json.toPrettyString();
    }
}